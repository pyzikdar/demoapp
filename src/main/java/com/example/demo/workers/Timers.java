/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.workers;

import com.example.demo.DemoApp;
import com.example.demo.service.ToolsSrv;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implementacja wątków uruchomienia
 *
 * @author Szefunio
 */
public class Timers {

    private ToolsSrv tools = new ToolsSrv();
    private Queue<Integer> numbersInTimers;
    private Timer runPrint;
    private Timer runGenApi;
    private Timer runGenInternal;

    public Timers(Queue numbersIn) {
        this.numbersInTimers = numbersIn;
    }

    public void runStopTask() {
        Timer endWork = new Timer();
        endWork.schedule(stopTask, DemoApp.END_WORK_AFTER);
    }

    public void runPrintSumTask() {
        runPrint = new Timer("print__worker");
        runPrint.scheduleAtFixedRate(printSumTask, new Date(), DemoApp.RUN_TIME_SUM);
    }

    private void stopPrintSumTaks() {
        System.out.println(tools.getNow() + "; Koniec przetwarzania");
        runPrint.cancel();
    }

    public void runGetDataFromApiTask() {
        runGenApi = new Timer("gen__API");
        runGenApi.scheduleAtFixedRate(getDataFromApiTask, new Date(), DemoApp.RUN_GET_DATA_FROM_API);
    }

    private void stopGetDataFromApiTask() {
        System.out.println(tools.getNow() + "; Koniec strzałów do API");
        runGenApi.cancel();
    }

    public void runGetDataFromInternalSrcTask() {
        runGenInternal = new Timer("gen__Internal");
        runGenInternal.scheduleAtFixedRate(getDataFromInternalSrc, new Date(), DemoApp.RUN_GET_DATA_FROM_INT_SRC);
    }

    private void stopGetDataFromInternalSrcTask() {
        System.out.println(tools.getNow() + "; Koniec generowania z wewnętrznego źródła");
        runGenInternal.cancel();
    }

    private TimerTask printSumTask = new TimerTask() {
        @Override
        public void run() {
            System.out.println(tools.getNow() + "; " + Thread.currentThread().getName()
                    + "; Sumka kolejnych " + numbersInTimers.size() + " liczb z kolejki: " + DemoApp.sum);
        }
    };

    private TimerTask getDataFromInternalSrc = new TimerTask() {
        @Override
        public void run() {
            System.out.println(tools.getNow() + "; " + Thread.currentThread().getName());
            int randomNumbers = tools.generateInt();
            numbersInTimers.offer(randomNumbers);
        }
    };

    private TimerTask getDataFromApiTask = new TimerTask() {
        @Override
        public void run() {
            System.out.println(tools.getNow() + "; " + Thread.currentThread().getName());
            List randomNumbers = tools.getIntFromApi();
            numbersInTimers.addAll(randomNumbers);
        }
    };

    private TimerTask stopTask = new TimerTask() {
        @Override
        public void run() {
            stopGetDataFromApiTask();
            stopGetDataFromInternalSrcTask();
            tools.stopThreads();
            stopPrintSumTaks();
            System.out.println(tools.getNow() + "; Suma wszystkich: " + DemoApp.sum);
            System.exit(0);
        }
    };

}
