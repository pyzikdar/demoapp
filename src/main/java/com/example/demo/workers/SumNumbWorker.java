/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.workers;

import com.example.demo.DemoApp;
import com.example.demo.service.ToolsSrv;
import java.util.Iterator;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Szefunio
 */
public class SumNumbWorker implements Runnable {

    private Queue<Integer> numbersInWorker;
    private ToolsSrv tools = new ToolsSrv();

    public SumNumbWorker(Queue numbersIn) {
        this.numbersInWorker = numbersIn;
    }

    @Override
    public void run() {

        Thread.currentThread().setName(Thread.currentThread().getName() + "; sum__Worker");
        while (DemoApp.exit == false) {
            if (numbersInWorker != null) {
                System.out.println(tools.getNow() + "; " + Thread.currentThread().getName()
                        + "; Poczatkowy stan kolejki:" + numbersInWorker);
                Iterator<Integer> it;
                for (it = numbersInWorker.iterator(); it.hasNext();) {
                    tools.sumNumbers(numbersInWorker.poll());
                }
                System.out.println(tools.getNow() + "; " + Thread.currentThread().getName()
                        + "; Bieżący stan kolejki:" + numbersInWorker);
            }
            try {
                Thread.sleep(DemoApp.RUN_TIME_SUM);
            } catch (InterruptedException ex) {
                Logger.getLogger(SumNumbWorker.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
