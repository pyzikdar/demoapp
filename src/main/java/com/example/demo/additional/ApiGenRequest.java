/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.additional;

import lombok.Data;

/**
 *
 * @author Szefunio
 */
@Data
public class ApiGenRequest {

    private final String API_VER = "\"jsonrpc\": 2.0";
    private final int API_REQUES_ID = 16052;
    private final String API_KEY = "f57779ba-da0c-471f-acd6-a73b53c888a4";
    private String methodName = "generateIntegers";
    private int paramN = 20;
    private int paramMin = 1;
    private int paramMax = 10;

    public String IntGenRequest() {
        String requetBody = "{" 
                + API_VER + ","
                + "\"method\":\"" + methodName + "\","
                + "\"params\":{\"apiKey\":\"" + API_KEY + "\","
                + "\"n\":" + paramN + ","
                + "\"min\":" + paramMin + ","
                + "\"max\":" + paramMax + "},"
                + "\"id\":" + API_REQUES_ID + "}";
        return requetBody;
    }

    public String IntGenRequest(String methodName, int paramN, int paramMin, int paramMax) {
        String requetBody = "{" 
                + API_VER + ","
                + "\"method\":\"" + methodName + "\","
                + "\"params\":{\"apiKey\":\"" + API_KEY + "\","
                + "\"n\":" + paramN + ","
                + "\"min\":" + paramMin + ","
                + "\"max\":" + paramMax + "},"
                + "\"id\":" + API_REQUES_ID + "}";
        return requetBody;
    }

}
