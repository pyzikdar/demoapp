package com.example.demo;

import java.util.LinkedList;
import java.util.Queue;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class DemoApp {

    public static Queue<Integer> mainNumbersIn = new LinkedList<>();
    public static int sum = 0;
    public static boolean exit = false;
    public static final int RUN_TIME_SUM = 2000;
    public static final int RUN_GET_DATA_FROM_API = 1000;
    public static final int RUN_GET_DATA_FROM_INT_SRC = 1000;
    public static final int END_WORK_AFTER = 30000;

    public static void main(String[] args) throws InterruptedException {
        new SpringApplicationBuilder(DemoApp.class)
                .web(WebApplicationType.NONE)
                .run(args);
        process();
    }

    private static void process() throws InterruptedException {
        SimpleWorker work = new SimpleWorker();
        work.runWorkers(mainNumbersIn);
    }
}
