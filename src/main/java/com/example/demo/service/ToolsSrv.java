/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.service;

import com.example.demo.additional.ApiGenRequest;
import com.example.demo.DemoApp;
import com.github.wnameless.json.flattener.JsonFlattener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

/**
 * Metody do wykonywania zadań
 * @author Szefunio
 */
@Service
public class ToolsSrv {

    private static final RestTemplate REST = new RestTemplate();
    private final String API_URL = "https://api.random.org/json-rpc/2/invoke";

    public ToolsSrv() {
    }

    public String getNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss.sss");
        return sdf.format(new Date());
    }
    
    public int generateInt() {
        Random r = new Random();
        int randomNumb = r.nextInt(10);
        return randomNumb;
    }

    public List<Integer> getIntFromApi() {
        List<Integer> integerList = new ArrayList();
        ResponseEntity result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String body = new ApiGenRequest().IntGenRequest();
        HttpEntity<String> requestBody = new HttpEntity<>(body, headers);
        try {
            result = REST.exchange(
                    API_URL, HttpMethod.POST, requestBody, String.class);
        } catch (RestClientResponseException ex) {
            System.out.println("problem ze strzałem do API" + ex);
        }

        if (result.getStatusCode() == HttpStatus.OK) {
            String jsonString = result.getBody().toString();
            Map<String, Object> flattenedJsonMap = JsonFlattener.flattenAsMap(jsonString);
            flattenedJsonMap.forEach((key, val) -> {
                if (key.contains("result.random.data")) {
                    integerList.add((int) val);
                }
            });
        } else {
            throw new RestClientResponseException("problem z API",
                    result.getStatusCodeValue(), result.getBody().toString(), null, null, null);
        }
        return integerList;
    }

    public void sumNumbers(int queValue) {
        DemoApp.sum = DemoApp.sum + queValue;
    }

    public void stopThreads() {
        DemoApp.exit = true;
    }
}
