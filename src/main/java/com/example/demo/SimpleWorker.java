/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo;

import com.example.demo.workers.Timers;
import com.example.demo.workers.SumNumbWorker;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Uruchamia równolegle poszczególne zadania
 * @author Szefunio
 */
public class SimpleWorker {

    ExecutorService threadPool = Executors.newFixedThreadPool(5);

    public void runWorkers(Queue mainNumbersIn) {
        Timers task = new Timers(mainNumbersIn);
        SumNumbWorker sumWorker = new SumNumbWorker(mainNumbersIn);
        task.runGetDataFromApiTask();
        task.runGetDataFromInternalSrcTask();
        threadPool.execute(sumWorker);
        task.runPrintSumTask();
        task.runStopTask();
    }
}
